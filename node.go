package main

// HTMLNode hold information about a html node
type HTMLNode struct {
	// Properties
	Type       string
	Properties map[string]string

	Parent   *HTMLNode
	Children []*HTMLNode
}
