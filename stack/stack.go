package stack

import (
	"errors"
	"sync"
)

// Stack is a simple interface stack struct
type Stack struct {
	lock sync.Mutex
	s    []interface{}
}

// New creates a new stack
func New() *Stack {
	return &Stack{sync.Mutex{}, make([]interface{}, 0)}
}

// Push adds an object to the stack
func (s *Stack) Push(v interface{}) {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.s = append(s.s, v)
}

// Pop returns the top object and removes it from the stack
func (s *Stack) Pop() (interface{}, error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	l := len(s.s)
	if l == 0 {
		return 0, errors.New("Empty Stack")
	}

	res := s.s[l-1]
	s.s = s.s[:l-1]
	return res, nil
}

// Len returns the length of the stack
func (s *Stack) Len() int {
	s.lock.Lock()
	defer s.lock.Unlock()

	return len(s.s)
}

// Peek returns the top object without removing it
func (s *Stack) Peek() (interface{}, error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	len := s.Len()
	if len == 0 {
		return nil, errors.New("stack empty")
	}

	return s.s[len-1], nil
}
