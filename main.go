package main

import (
	"io"
	"log"
	"strings"

	"gitlab.com/ivelin.penchev/html-queryselector/stack"
	"golang.org/x/net/html"
)

func main() {
	h := strings.NewReader(`<!DOCTYPE html>
	<html>
	<body>
	
	<H1 data="ytolo" mysql>My First
	<input asdasda >
	<p> YOLO  Heading</h1>
	<p>My first paragraph.</p>
	
	</body>
	</html>`)

	stack := stack.New()
	z := html.NewTokenizer(h)

	for {
		if z.Next() == html.ErrorToken {
			// Returning io.EOF indicates success.
			log.Println(z.Err() == io.EOF)
			return
		}
		token := z.Token()
		log.Println(token.Type)
		log.Println(token.Attr)
		log.Println(token.Data)
		log.Println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>")
		node, err := handleToken(token)
		if err != nil {
			return
		}

		stack.Push(node)
	}
}

func handleToken(token html.Token) (HTMLNode, error) {
	return HTMLNode{}, nil
}
